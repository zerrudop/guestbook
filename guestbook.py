from turtle import *
from tkinter import *
from tkinter import font
import random

root = Tk()
root.title('Font Families')
fonts=list(font.families())
fonts.sort()
print(fonts)

colors = ["red","blue","black","purple","green"]

penup()
goto(-250,200)
write("Welcome to my Guestbook", font=("Comic Sans MS", 30, "normal"))
goto(-500,200)
count = 0
while True:
    pencolor("black")
    name = textinput("Name", "Please enter your name")
    font = random.choice(fonts)
    if name == "quit":
        break    
    else:
        message = textinput(font, "Leave your message")
        output = name +": " + message
        penup()
        right(90)
        forward(50)
        left(90)
        pendown()
        forward(450)
        right(180)
        forward(450)
        right(180)
        pencolor(random.choice(colors))
        write(output, font=(font, 16))

        count += 1

        if count == 10:
            penup()
            goto(0, 200)
        elif count == 20:
            break
    

done()
